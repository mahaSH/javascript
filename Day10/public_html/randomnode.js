const express = require('express');
const app = express();
const port = 3000;

app.get('/', (request, response) => {
    response.send('Hello from Express!');
});

var count = 0;
app.get('/count', (request, response) => {
    response.send('<p>Count is <b>' + count++ + '</b></p>');
});

var randNumList = [];

app.get('/genrand', (request, response) => {
    var min = request.query['min'];
    var max = request.query['max'];
    var rand = Math.floor(Math.random() * 6) + 1;
    randNumList.push(rand);
    response.send("" + rand);//force to send a number as string
});

app.get('/histoty', (request, response) => {
    var html;
    for(var i=0;i<randNumList.length;++i){
       html="<li>"+randNumList[i]+"</li>"     
    }
 

    response.send("html");
});

app.get('/history', (request, response) => {
    var html = "";
    // loop over randNumList, generate <li> for each and append to html
    response.send(html);
});


app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err);
    }

    console.log(`server is listening on ${port}`);
});

