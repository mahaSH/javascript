const express = require('express');
const fs = require('fs');
const app = express();

const port = 3005;
const jsonDataFile = 'chatlines.json';


var messagesArray = [];

function saveAllData() {
    var dataStr = JSON.stringify(messagesArray, null, " ");
    fs.writeFileSync(jsonDataFile, dataStr);
}

function loadAllData() {
    if (fs.existsSync(jsonDataFile)) {
        var dataStr = fs.readFileSync(jsonDataFile);
        messagesArray = JSON.parse(dataStr);
    }
}

loadAllData();

app.get('/send', (request, response) => {
    var name = request.query['name'];
    var color = request.query['color'];
    var msg = request.query['msg'];
    console.log(`/send name=${name}, color=${color}, msg=${msg}`);
    // TODO: verify all 3 parameters look valid
    if (name == undefined || name == '' || color == '' || msg == '') {
        response.send("Error: invalid data sent");
        return;
    }
    var chatLine = {name: name, color: color, msg: msg };
    messagesArray.push(chatLine);
    saveAllData();
    response.send(`Message sent!`);
});

app.get('/update', (request, response) => {
    var html = "";
    console.log("update");
    for (var i = 0; i < messagesArray.length; i++) {
        var m = messagesArray[i];
        html += `<option style="color: ${m.color}"><b>${m.name}:</b> ${m.msg}</option>`;
    }
    response.send(html);
});

app.get('/statistics', (request, response) => {
    usersUnique = [];
    usersMsgsCounts = { };
    usersCharsCounts = { };
    
    for (var i = 0; i < messagesArray.length; i++) {
        var m = messagesArray[i];
        if (usersUnique.includes(m.name)) {
            usersMsgsCounts[m.name]++;
        } else {
            usersUnique.push(m.name);
            usersMsgsCounts[m.name] = 1;
        }
    }
    var html = "";
    for (var i = 0; i < usersUnique.length; i++) {
        var name = usersUnique[i];
        var msgsCount = usersMsgsCounts[name];
        html += `<tr><td class="name">${name}</td><td class="totalMsgs">${msgsCount}</td></tr>` + "\n";
    }
    response.send(html);
});

app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err);
    }

    console.log(`server is listening on ${port}`);
});



