const express = require('express');
const fs = require('fs');
const app = express();

const port = 3003;
const jsonDataFile = 'data.json';


var friendsArray = [];

function saveAllData() {
    var dataStr = JSON.stringify(friendsArray, null, " ");
    fs.writeFileSync(jsonDataFile, dataStr);
}

function loadAllData() {
    if (fs.existsSync(jsonDataFile)) {
        var dataStr = fs.readFileSync(jsonDataFile);
        friendsArray = JSON.parse(dataStr);
    }
}

loadAllData();

app.get('/add', (request, response) => {
    var id = request.query['id'] - 0;
    var name = request.query['name'];
    var age = request.query['age'] - 0;
    // TODO: verify all 3 parameters look valid
    if (id < 1 || name.length < 1 || age < 1) {
        response.send("error");
        return;
    }
    var friend = {id: id, name: name, age: age};
    friendsArray.push(friend);
    saveAllData();
    response.send(`friend with id=${id} created`);
});

app.get('/delete', (request, response) => {
    var id = request.query['id'] - 0;
    for (var i = 0; i < friendsArray.length; i++) {
        if (friendsArray[i].id == id) {
            friendsArray.splice(i, 1);
            saveAllData();
            response.send(`friend with id=${id} deleted`);
            return;
        }
    }
    response.send(`error deleting friend, id=${id} not found`);
});

app.get('/fetchall', (request, response) => {
    var html = "";
    console.log("FETCHALL CALLED");
    for (var i = 0; i < friendsArray.length; i++) {
        var f = friendsArray[i];
        html += `<option value="${f.id}">${f.id}: ${f.name} is ${f.age} y/o</option>`;
    }
    response.send(html);
});

app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err);
    }

    console.log(`server is listening on ${port}`);
});


