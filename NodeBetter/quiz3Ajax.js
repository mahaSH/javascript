/*
Developed by Maha M.Shawkat
*/
const express = require('express');
const fs = require('fs');
const app = express();

const port = 3005;
const jsonDataFile = 'travellers.json';


var travelArray = [];
var error;

function saveAllData() {
    var dataStr = JSON.stringify(travelArray, null, " ");
    fs.writeFileSync(jsonDataFile, dataStr);
}

function loadAllData() {
    if (fs.existsSync(jsonDataFile)) {
        var dataStr = fs.readFileSync(jsonDataFile);
        travelArray = JSON.parse(dataStr);
    }
}

loadAllData();

app.get('/add', (request, response) => {
    error = "";
    var id = request.query['id']
    var passport = request.query['passport'];
    var code = request.query['code'];
    var city = request.query['city'];
    var departure = request.query['departure'];
    var arrival = request.query['arrival'];
    //check the passport
    if (passport == "" || /^[A-Z]{2}\d{6}$/.test(passport)) {
        error += "passport should be 2 capital letters followed by 6 digits \n";;
    }
    //check airport code
    if (code == "" || !/^[A-Z]{3}$/.test(code)) {
        error += "airport code  should be 3 capital  \n";

    }
    //check the city
    if (city == "" || !/^[A-Za-z.\s_-]+$/.test(city) || city.length > 20 || city.length < 1) {
        error += "city should be 1-20 characters  \n";

    }
    //check dates
    if (!departure || !arrival) {
        error += "please enter the dates correctly  \n";


    }
    if (error != "") {
        response.send(error);
        return;
    }
    var traveller = {id: id, passport: passport, code: code, city: city, departure: departure, arrival: arrival};
    travelArray.push(traveller);
    saveAllData();
    response.send(`Item added`);
});
app.get('/getList', (request, response) => {
    var html = "";
    console.log("getList");
    for (var i = 0; i < travelArray.length; i++) {
        var t = travelArray[i];
        html += `<option >${t.id}: ${t.passport} travels to${t.city} through ${t.code} airpot on ${t.departure} and will arrive on ${t.arrival}</option>`;
    }
    response.send(html);
});

//delete option
app.get('/delete', (request, response) => {
    for (var i = 0; i < travelArray.length; i++) {
        var t = travelArray[i];
        if (t.id == id) {
            travelArray.splice(t, 10)
        }
    }
    saveAllData();
});
app.get('/getlistastable', (request, response) => {
    travUnique = [];
    travtripCount = { };
    
    for (var i = 0; i < travelArray.length; i++) {
        var m = travelArray[i];
        if (travUnique.includes(m.passport)) {
            travtripCount[m.passport]++;
        } else {
            travUnique.push(m.passport);
            travtripCount[m.passport] = 1;
        }
    }
    var html = "";
   
    for (var i = 0; i < travUnique.length; i++) {
        var t = travUnique[i];
        html += `<tr><td >${t.id}</td ><td >${t.passport} <td ><td >${t.city}<td > <td > ${t.code}<td > <td > ${t.departure}<td > <td > ${t.arrival}</td ></tr>`
    }
    response.send(html);
    
  });
  
app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err);
    }

    console.log(`server is listening on ${port}`);
});




