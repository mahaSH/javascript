    const express = require('express');
    const fs = require('fs');
    const app = express();

    const port = 3005;
    const jsonDataFile = 'chatlines.json';


    var messagesArray = [];
    var chaterArray=[];
    function saveAllData() {
        var dataStr = JSON.stringify(messagesArray, null, " ");
        fs.writeFileSync(jsonDataFile, dataStr);
    }

    function loadAllData() {
        if (fs.existsSync(jsonDataFile)) {
            var dataStr = fs.readFileSync(jsonDataFile);
            messagesArray = JSON.parse(dataStr);
        }
    }

loadAllData();

app.get('/send', (request, response) => {
    var name = request.query['name'];
    var color = request.query['color'];
    var msg = request.query['msg'];
    console.log(`/ADD name=${name}, color=${color}, msg=${msg}`);
    // TODO: verify all 3 parameters look valid
    if (name == undefined || name == '' || color == '' || msg == '') {
        response.send("Error: invalid data sent");
        return;
    }
    var chatLine = {name: name, color: color, msg: msg };
    messagesArray.push(chatLine);
    saveAllData();
    response.send(`Message sent!`);
});

app.get('/delete', (request, response) => {
    var id = request.query['id'] - 0;
    for (var i = 0; i < messagesArray.length; i++) {
        if (messagesArray[i].id == id) {
            messagesArray.splice(i, 1);
            saveAllData();
            response.send(`friend with id=${id} deleted`);
            return;
        }
    }
    response.send(`error deleting friend, id=${id} not found`);
});

app.get('/fetchall', (request, response) => {
    var html = "";
    for (var i = 0; i < messagesArray.length; i++) {
        var f = messagesArray[i];
        html += `<option value="${i+1}">${f.name}: ${f.msg} </option>`;
    }
    response.send(html);
});
app.get('/statistics', (request, response) => {
    var html = "<tr>";
    
    for (var i = 0; i < messagesArray.length; i++) {
        var f = messagesArray[i];
        var counter=0;
        var words=0;
        for (var j = 0; j < messagesArray.length; j++){
          if (messagesArray[j].name==messagesArray[i].name){
              counter++;
              var msg=(messagesArray[i].msg);
              var length=msg.length
              words+=length;
          }  
          var person={name:messagesArray[i].name,chats:counter,words:words}
          chaterArray.push(person);
        } 
    }
    //remove duplications
    for(var i = 0; i < chaterArray.length; i++){
        var counter=0;
    for(var j = 0; j < chaterArray.length; j++){
        if (chaterArray[i].name==chaterArray[j].name&&counter>1){
        chaterArray.splice(chaterArray[i],1);
        break;
        }
        counter++;
    }
    }
    for(var i = 0; i < chaterArray.length; i++){
         var f = chaterArray[i];
        html += `<td>${f.name}</td> <td>${f.msg}</td><td>${f.words}</td> </option>`;
    }
    response.send(html);
});

app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err);
    }

    console.log(`server is listening on ${port}`);
});


